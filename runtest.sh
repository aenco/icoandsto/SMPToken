#!/usr/bin/env bash
fswatch -0 ./test | while read -d "" event; \
do \
    truffle test
done

fswatch -0 ./contracts | while read -d "" event; \
do \
    clear
    echo "---------------Deployment Phase---------------"
    truffle migrate -reset
    echo "-----------------Testing Phase----------------"
    truffle test
done