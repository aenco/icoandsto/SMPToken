module.exports = {
  networks: {
    develop: {
      host: "127.0.0.1",
      port: 9545,
      network_id: "1", // Match any network id
      gas: 8700000
    },
    development: {
      host: "127.0.0.1",
      port: 9545,
      network_id: "1", // Match any network id
      gas: 8700000
    },
      other: {
          host: "127.0.0.1",
          port: 9545,
          network_id: "1", // Match any network id
          gas: 8700000,
          accounts: 15,
          defaultEtherBalance: 5000,
          blockTime:5
      }
  }
};